<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'الترخيص:',

	// M
	'mis_a_jour' => 'تم التحديث:',
	'mots_cles' => 'المفاتيح',

	// P
	'publie_le' => 'نُشر في:',

	// T
	'traductions' => 'الترجمات:',
];

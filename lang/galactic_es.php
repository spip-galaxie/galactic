<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'Licencia:',

	// M
	'mis_a_jour' => 'Actualizado:',
	'mots_cles' => 'Palabras clave',

	// P
	'publie_le' => 'Publicado el:',

	// T
	'traductions' => 'Traducciones:',
];

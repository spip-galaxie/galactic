<?php
/**
 * Options au chargement du plugin Squelette SPIP
 *
 * @plugin     Squelette SPIP
 * @copyright  2017
 * @author     Jordan, nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Skelspip\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// correction des intertitres
$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "</h2>\n";

/*
|----------------------------|
| header                     |
|----------------------------|
|                   |        |
| content           | nav    |
|                   |        |
|                   |        |
|                   |        |
|                   |--------|
|                   | aside  |
|-------------------|        |
| extra             |        |
|                   |        |
|----------------------------|
| footer                     |
|----------------------------|
*/

// définition des blocs Z
$GLOBALS['z_blocs'] = array(
	'content',
	'head',
	'head_js',
	'header',
	'breadcrumb',
	'extra',
	'nav',
	'aside',
	'footer',
);

define('_URLS_PROPRES_MIN', 2); // pour fr/ et en/

define('PLUGIN_COLORATION_CODE_TAB_WIDTH', 4);
define('PLUGIN_COLORATION_CODE_STYLES_INLINE', false); // des classes CSS, svp
define('PLUGIN_COLORATION_CODE_SANS_STYLES', true); // styles dans un fichier css specifique

// stocker la langue d'arrivee pour que le sommaire affiche la langue souhaitee
// et on ajoute la langue dans le contexte systematiquement.
if (!$langue = _request('lang')) {
	if (empty($_COOKIE['spip_lang']) or !$langue = $_COOKIE['spip_lang']) {
		include_spip('inc/lang');
		$langues = explode(',', $GLOBALS['meta']['langues_multilingue']);
		$langue = utiliser_langue_visiteur();
		if (!in_array($langue, $langues)) {
			$langue = $GLOBALS['meta']['langue_site'];
		}
	}
	set_request('lang', $langue);
}

// stocker la langue...
if (empty($_COOKIE['spip_lang']) or $langue != $_COOKIE['spip_lang']) {
	include_spip('inc/cookie');
	spip_setcookie('spip_lang', $langue);
}

if(!window.console) {
	window.console = {
		log: function(str) {
			return
		}
	};
}

$(function() {
	globalPage.init();
});

var globalPage = (function() {
	var that = {};

	that.init = function() {

		// liens sortant et PDF en target _blank + ajout d'une mention dans le title ou alt des images (accessibilité)
		$('a.spip_out, a.spip_url, a.spip_glossaire, a[href$=".pdf"], a[href$=".doc"]').each(function() {
			$(this).attr('target', '_blank').attr('rel', 'noreferrer noopener');
			if($(this).attr('title') && $(this).attr('title').length) {
				$(this).attr('title', $(this).attr('title') + ' - Nouvelle fenêtre');
			} else if($(this).text().trim().length) {
				$(this).attr('title', 'Nouvelle fenêtre');
			} else {
				$(this).find('img').each(function() {
					$(this).attr('alt', $(this).attr('alt') + ' - Nouvelle fenêtre');
				});
			}
		});

		// gestion du "faux select" sur le menu de langue
		var $menu_lang_ = $('.menu-lang');
		var $menu_lang__select = $('.menu-lang__select');
		var $menu_lang__items  = $('.menu-lang__items');
		$menu_lang__select.on('click', function() {
			$menu_lang_.toggleClass('open');
			$menu_lang__items.slideToggle('fast');
		});
		$menu_lang__items.find('a').on('click', function(e) {
			$menu_lang__select.text($(this).text());
			$menu_lang__items.hide();
			$(this).parent('li').addClass('on');
		});

	};

	return that;

})();


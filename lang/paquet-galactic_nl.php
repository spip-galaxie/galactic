<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-galactic?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// G
	'galactic_nom' => 'Galactic',
	'galactic_slogan' => 'Een skelet voor de sites van de galaxie',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-galactic?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// G
	'galactic_description' => '',
	'galactic_nom' => 'Galactic',
	'galactic_slogan' => 'Un squelette pour les sites de la galaxie',
];

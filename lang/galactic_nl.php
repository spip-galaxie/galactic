<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// E
	'evitement_contenu' => 'Ga naar de inhoud',
	'evitement_navigation' => 'Ga naar de navigatie',

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'Licentie:',

	// M
	'mis_a_jour' => 'Aangepast:',
	'mots_cles' => 'Trefwoorden',

	// P
	'publie_le' => 'Gepubliceerd op:',

	// R
	'recherche' => 'Zoeken',

	// T
	'traductions' => 'Vertalingen:',

	// V
	'vous_etes_ici' => 'Je bent hier:',
];

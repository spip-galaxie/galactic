<?php
/**
 * Définit les autorisations du plugin Squelette SPIP
 *
 * @plugin     Squelette SPIP
 * @copyright  2017
 * @author     Jordan, nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Skelspip\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function galactic_autoriser() {
}

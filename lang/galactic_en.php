<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'evitement_contenu' => 'Go to content',
	'evitement_navigation' => 'Go to navigation',

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'License :',

	// M
	'mis_a_jour' => 'Updated :',
	'mots_cles' => 'Keywords',

	// P
	'publie_le' => 'Published :',

	// R
	'recherche' => 'Search',

	// T
	'traductions' => 'Translations :',

	// V
	'vous_etes_ici' => 'You are here:',
];

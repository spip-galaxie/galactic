<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// E
	'evitement_contenu' => 'Aller au contenu',
	'evitement_navigation' => 'Aller à la navigation',

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'Licence :',

	// M
	'mis_a_jour' => 'Mis à jour :',
	'mots_cles' => 'Mots clés',

	// P
	'publie_le' => 'Publié le :',

	// R
	'recherche' => 'Recherche',

	// T
	'traductions' => 'Traductions :',

	// V
	'vous_etes_ici' => 'Vous êtes ici :',
];

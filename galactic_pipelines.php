<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Insertion dans le pipeline pre_boucle (SPIP)
 *
 * Tri des articles et rubriques par "num titre, titre" par défaut
 *
 * @param Object $boucle
 * @return Object
 */
function galactic_pre_boucle($boucle){
	if (in_array($boucle->type_requete, array('rubriques','articles'))
		AND !$boucle->order) {
		$boucle->select[] = "0+" . $boucle->id_table . ".titre AS autonum";
		$boucle->order[]  = "'autonum'";
		$boucle->order[]  = "'" . $boucle->id_table . ".titre'";
	}
	return $boucle;
}
